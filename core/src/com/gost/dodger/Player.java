package com.gost.dodger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class Player extends Actor
{
	private float x;
	private float y;
	private float startX;
	private float startY;
	private int srcX;
	private int srcY;
	private float height;
	private float width;
	private int speed;
	private boolean isDie;
	private int deathCounter;
	private int shootCounter;
	private Map<String, Sound> sounds = new HashMap<String, Sound>();
	private List<Asteroid> asteroidsList;
	private BitmapFont font;
	private List<Bullet> bulletsList = new ArrayList<Bullet>(8);

	private static Texture playerTexture = new Texture(Gdx.files.internal("ship.png"));

	public Player(List<Asteroid> asteroidsList, float x, float y)
	{
		this.x = x;
		this.y = y;
		this.asteroidsList = asteroidsList;
		startX = x;
		startY = y;
		height = 120;
		width = 96;
		srcX = (int) (width * 2);
		speed = 500;
		font = new BitmapFont();
		sounds.put("explosion", Gdx.audio.newSound(Gdx.files.internal("explosion.wav")));
	}

	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		if (!isDie && shootCounter < 0){
			if (Gdx.input.isKeyPressed(Keys.SPACE))
			{
				shoot();
				shootCounter = 10;
			}
		} else
		{
			shootCounter--;
		}
		for (int i = 0; i < bulletsList.size(); i++)
		{
			if (bulletsList.get(i).isGone())
			{
				bulletsList.remove(i);
				i--;
			}
		}
		for (Bullet bullet : bulletsList)
		{
			bullet.draw(batch, parentAlpha);
		}

		if (isDie(asteroidsList, batch))
		{
			deathCounter = 300;
		}
		if (deathCounter > 0)
		{
			font.draw(batch, "YOU DIED!!! :D", Gdx.graphics.getHeight() / 2, Gdx.graphics.getWidth() / 2, 256, 256,
					false);
			deathCounter--;
		} else
		{
			isDie = false;
			srcX = (int) (width * 2);
			if (Gdx.input.isKeyPressed(Keys.DPAD_LEFT))
			{
				if (x >= 0)
				{
					x -= Gdx.graphics.getDeltaTime() * speed;
					srcX = (int) (width);
				}
			}
			if (Gdx.input.isKeyPressed(Keys.DPAD_RIGHT))
			{
				if (x <= Gdx.graphics.getWidth() - width)
				{
					x += Gdx.graphics.getDeltaTime() * speed;
					srcX = (int) (width * 4);
				}
			}
			if (Gdx.input.isKeyPressed(Keys.DPAD_UP))
			{
				if (y <= Gdx.graphics.getHeight() - height)
				{
					y += Gdx.graphics.getDeltaTime() * speed;
				}
			}
			if (Gdx.input.isKeyPressed(Keys.DPAD_DOWN))
			{
				if (y >= 0)
				{
					y -= Gdx.graphics.getDeltaTime() * speed;
				}
			}

			batch.draw(playerTexture, getX(), getY(), srcX, srcY, (int) width, (int) height);
		}
	}

	public float getX()
	{
		return x;
	}

	public void setX(float x)
	{
		this.x = x;
	}

	public float getY()
	{
		return y;
	}

	public void setY(float y)
	{
		this.y = y;
	}

	public int getSrcX()
	{
		return srcX;
	}

	public void setSrcX(int srcX)
	{
		this.srcX = srcX;
	}

	public int getSrcY()
	{
		return srcY;
	}

	public void setSrcY(int srcY)
	{
		this.srcY = srcY;
	}

	public float getHeight()
	{
		return height;
	}

	public void setHeight(float height)
	{
		this.height = height;
	}

	public float getWidth()
	{
		return width;
	}

	public void setWidth(float width)
	{
		this.width = width;
	}

	public static Texture getPlayerTexture()
	{
		return playerTexture;
	}

	public static void setPlayerTexture(Texture playerTexture)
	{
		Player.playerTexture = playerTexture;
	}

	public boolean isDie(List<Asteroid> list, Batch batch)
	{
		double powX;
		double powY;
		if (!isDie)
		{
			for (Asteroid asteroid : list)
			{
				powX = Math.pow(x - asteroid.getX(), 2);
				powY = Math.pow(y - asteroid.getY(), 2);
				if (Math.sqrt(powX + powY) < 70)
				{
					sounds.get("explosion").play();
					x = startX;
					y = startY;
					isDie = true;
					return true;
				}
			}
		}
		return false;
	}

	public void shoot()
	{
		if (bulletsList.size() < 9)
		{
			bulletsList.add(new Bullet(asteroidsList, x + 32, y));
		}
	}

	public List<Bullet> getBulletsList()
	{
		return bulletsList;
	}
}
