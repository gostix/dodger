package com.gost.dodger;

import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class Asteroid extends Actor
{
	private float x;
	private float y;
	private int srcX;
	private int srcY;
	private float height;
	private float width;
	private int speed;
	private boolean isUp;
	private boolean isDown;
	private boolean isLeft;
	private boolean isRight;
	private float rotaion;
	private float[] roatate = { -0.1f, -0.2f, -0.3f, 0.1f, 0.2f, 0.3f };

	private Texture asteroidTexture = new Texture(Gdx.files.internal("asteroids.png"));
	private Sprite asteroidSprite = new Sprite(asteroidTexture);

	Pixmap  pixmap;
	
	public Asteroid(float x, float y)
	{
		Random random = new Random();
		this.x = x;
		this.y = y;
		height = 65.0f;
		width = 72.0f;
		srcX = (int) (width * 2);
		speed = random.nextInt(150);
		rotaion = roatate[random.nextInt(5)];
		if (random.nextBoolean())
		{
			isUp = true;
		} 
		else
		{
			isDown = true;
		}
		if (random.nextBoolean())
		{
			isRight = true;
		}
		else
		{
			isLeft = true;
		}
	}

	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		if (x >= 0 && isLeft)
		{
			x -= Gdx.graphics.getDeltaTime() * speed;
		} else
		{
			isRight = true;
			isLeft = false;
		}

		if (x <= Gdx.graphics.getWidth() - width && isRight)
		{
			x += Gdx.graphics.getDeltaTime() * speed;
		} 
		else
		{
			isRight = false;
			isLeft = true;
		}

		if (y <= Gdx.graphics.getHeight() - height && isUp)
		{
			y += Gdx.graphics.getDeltaTime() * speed;
		} 
		else
		{
			isUp = false;
			isDown = true;
		}

		if (y >= 0 && isDown)
		{
			y -= Gdx.graphics.getDeltaTime() * speed;
		} 
		else
		{
			isUp = true;
			isDown = false;
		}
		
		
		asteroidSprite.setOrigin(width / 2, height / 2);
		asteroidSprite.rotate(rotaion);
		asteroidSprite.setRegion(0, 0, (int) width, (int) height);
		asteroidSprite.setBounds(x, y, width, height);
		asteroidSprite.draw(batch);
	}

	public float getX()
	{
		return x;
	}

	public void setX(float x)
	{
		this.x = x;
	}

	public float getY()
	{
		return y;
	}

	public void setY(float y)
	{
		this.y = y;
	}

	public int getSrcX()
	{
		return srcX;
	}

	public void setSrcX(int srcX)
	{
		this.srcX = srcX;
	}

	public int getSrcY()
	{
		return srcY;
	}

	public void setSrcY(int srcY)
	{
		this.srcY = srcY;
	}

	public float getHeight()
	{
		return height;
	}

	public void setHeight(float height)
	{
		this.height = height;
	}

	public float getWidth()
	{
		return width;
	}

	public void setWidth(float width)
	{
		this.width = width;
	}

	public Sprite getAsteroidTexture()
	{
		return asteroidSprite;
	}

	public void setAsteroidTexture(Sprite asteroidSprite)
	{
		this.asteroidSprite = asteroidSprite;
	}
}
