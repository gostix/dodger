package com.gost.dodger;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class Main extends Game
{
	private SpriteBatch batch;
	private Dodger dodger;
	private static Main main = new Main();

	private Main()
	{
	}

	@Override
	public void create()
	{
		batch = new SpriteBatch();
		dodger = new Dodger(batch);
		setScreen(dodger);
	}

	public static Main getInstance()
	{
		return main;
	}

	public void showPoker()
	{
		setScreen(dodger);
	}
}
