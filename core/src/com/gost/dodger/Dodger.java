package com.gost.dodger;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;

public class Dodger implements Screen
{
	SpriteBatch batch;
	Texture img;
	Stage stage;
	Player player;
	List<Asteroid> asteroidsList;
	
	public Dodger(SpriteBatch batch)
	{
		this.batch = batch;
		asteroidsList = addAsteroids(10);

		player = new Player(asteroidsList, 100, 100);
		img = new Texture("space.jpg");
		stage = new Stage(new StretchViewport(Gdx.graphics.getWidth(),
				Gdx.graphics.getHeight()), batch);
		Sound mp3Sound = Gdx.audio.newSound(Gdx.files.internal("space.mp3"));
		mp3Sound.loop();
		stage.addActor(player);
		for(Asteroid asteroid : asteroidsList)
		{
			stage.addActor(asteroid);
		}
	}

	@Override
	public void render(float delta)
	{
		Gdx.gl.glClearColor(1, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		batch.begin();
		batch.draw(img, 0,0);
		batch.end();
		
		stage.draw();
		stage.act(delta);

	}

	private List<Asteroid> addAsteroids(int count){
		List<Asteroid> asteroidsList = new ArrayList<Asteroid>();
		Random randomX = new Random();
		Random randomY = new Random();
		for(int i = 0; i < count; i++)
		{
			asteroidsList.add(new Asteroid(randomX.nextInt(Gdx.graphics.getWidth()), randomY.nextInt(Gdx.graphics.getWidth())));	
		}
		return asteroidsList;
	}
	
	@Override
	public void resize(int width, int height)
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void show()
	{
		Gdx.input.setInputProcessor(stage);

	}

	@Override
	public void hide()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void pause()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void resume()
	{
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose()
	{
		// TODO Auto-generated method stub

	}
}
