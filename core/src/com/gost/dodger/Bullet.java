package com.gost.dodger;

import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class Bullet extends Actor
{
	private float x;
	private float y;
	private int srcX;
	private int srcY;
	private float height;
	private float width;
	private int speed;
    private boolean isGone;
	private List<Asteroid> asteroidsList;

	private Texture bulletTexture = new Texture(Gdx.files.internal("bullet.png"));
	private Sprite bulletSprite = new Sprite(bulletTexture);

	Pixmap  pixmap;
	

	public Bullet(List<Asteroid> asteroidsList, float x, float y)
	{
		this.x = x;
		this.y = y;
		height = 32.0f;
		width = 32.0f;
		speed = 800;

		this.asteroidsList = asteroidsList;

	}

	@Override
	public void draw(Batch batch, float parentAlpha)
	{
		if(isGone || y > Gdx.graphics.getHeight() + 32)
		{
			isGone = true;
			return;
		}
		y += Gdx.graphics.getDeltaTime() * speed;
		bulletSprite.setRegion(0, 0, (int) width, (int) height);
		bulletSprite.setBounds(x, y, width, height);
		bulletSprite.draw(batch);
		isHitted(asteroidsList);
	}

	public float getX()
	{
		return x;
	}

	public void setX(float x)
	{
		this.x = x;
	}

	public float getY()
	{
		return y;
	}

	public void setY(float y)
	{
		this.y = y;
	}

	public int getSrcX()
	{
		return srcX;
	}

	public void setSrcX(int srcX)
	{
		this.srcX = srcX;
	}

	public int getSrcY()
	{
		return srcY;
	}

	public void setSrcY(int srcY)
	{
		this.srcY = srcY;
	}

	public float getHeight()
	{
		return height;
	}

	public void setHeight(float height)
	{
		this.height = height;
	}

	public float getWidth()
	{
		return width;
	}

	public void setWidth(float width)
	{
		this.width = width;
	}

	public Sprite getAsteroidTexture()
	{
		return bulletSprite;
	}

	public void setAsteroidTexture(Sprite asteroidSprite)
	{
		this.bulletSprite = asteroidSprite;
	}
	
	public boolean isHitted(List<Asteroid> list)
	{
		double powX;
		double powY;
		for (int i = 0; i < list.size(); i++)
		{
			powX = Math.pow(x - list.get(i).getX(), 2);
			powY = Math.pow(y - list.get(i).getY(), 2);
			if (Math.sqrt(powX + powY) < 32)
			{
				list.get(i).setVisible(false);
				list.remove(i);
				isGone = true;
				return true;
			}

		}
		return false;
	}
	
	public boolean isGone()
	{
		return isGone;
	}
	
}
